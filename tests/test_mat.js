var autonomy = require('ardrone-autonomy');
var mission  = autonomy.createMission();

mission.takeoff()
       .zero()
       //.go(x: 1, y: 0, z: 0, yaw: 90)      // Sets the current state as the reference
       //.altitude(2)  // Climb to altitude = 1 meter
       .forward(1)
       //.go({x:0,y:0})
       //.right(2)
       //.backward(2)
       //.left(2)
       .hover(20000)  // Hover in place for 1 second
       .land();

mission.run(function (err, result) {
    if (err) {
        console.trace("Oops, something bad happened: %s", err.message);
        mission.client().stop();
        mission.client().land();
    } else {
        console.log("Mission success!");
        process.exit(0);
    }
});
